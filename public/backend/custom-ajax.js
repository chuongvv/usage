$(function() {
    //hang on event of form with id=myform
    $(".submit_register").submit(function(e) {
        //prevent Default functionality
        e.preventDefault();
        //get the action-url of the form
        const _token = $("input[name='_token']").val();
        const name = $(".name").val();
        const email = $(".email").val();
        const tel = $(".tel").val();
        const gender = $('.gender').val();
        const password = $('.password').val();
        const password_confirmation = $('.password_confirmation').val();
        const birthday = $('.birthday').val();
        const address = $('.address').val();

        const APP_URL = "http://localhost:8000";
        $.ajax({
            url: APP_URL+"/api/register",
            type:'POST',
            data: {_token:_token, name:name, email:email, tel:tel, address:address,gender:gender,password:password,password_confirmation:password_confirmation,birthday:birthday},
            success: function(data) {
                if($.isEmptyObject(data.error)){
                    toastr.success(data.success);
                }else{
                    printErrorMsg(data.error);
                }
                /*if(data['status'] == 400){
                    if(data['status']['tel'])
                }*/

            }

        });

    });

});
function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}
$(function (){
    $(".submit_login").submit(function(e) {
        //prevent Default functionality
        e.preventDefault();
        //get the action-url of the form
        const _token = $("input[name='_token']").val();
        const email = $(".email").val();
        const password = $('.password').val();
        const APP_URL = "http://localhost:8000";
        $.ajax({
            url: APP_URL+"/api/login",
            type:'POST',
            data: {_token:_token, email:email, password:password},
            success: function(data) {
                if($.isEmptyObject(data.error)){
                    toastr.success(data.success);
                }else{
                    printErrorMsg(data.error);
                }
                /*if(data['status'] == 400){
                    if(data['status']['tel'])
                }*/

            }

        });

    });
});
