<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    //
    public function register(Request $request){
        $rules = array('name'=>'required|max:255','email' => "required|email",
            'password' => 'required|min:8|confirmed','password_confirmation'=>'required',
            'birthday'=>'required|before:today','gender'=>'required','address'=>'required|max:255','tel'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/');
        $validator = Validator::make($request->all(), $rules);

// Validate the input and return correct response
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);

        }
        else {
            $user = new User();
            $user->fill($request->all());
            $user->password = Hash::make($request->password);
            $user->save();
            $user->assignRole('employee');
            return response()->json(['success' => 'Successfully!']);
        }
    }

}
